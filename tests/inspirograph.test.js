const { getElementText, expectElementToExist } = require('./util');

describe('Inspiral Web', () => {
  let response;

  describe('main page', () => {
    beforeAll(async () => {
      response = await page.goto('https://inspiral-web.nathanfriend.com');
    });

    it('returns a 200', () => {
      expect(response.status()).toBe(200);
    });

    it('renders a gear', async () => {
      await expectElementToExist('.gear.ring-gear path');
    });
  });
});
