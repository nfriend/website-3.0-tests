const { getElementText } = require('./util');
const axios = require('axios').default;

describe('Smoke tests', () => {
  describe('web pages', () => {
    it.each`
      url                                                | title                                | selector                           | text
      ${'https://nathanfriend.com'}                      | ${'Nathan Friend'}                   | ${'h1'}                            | ${'Nathan Friend'}
      ${'https://nathanfriend.com/about-me'}             | ${'About Me'}                        | ${'.content-section h1'}           | ${'About Me'}
      ${'https://nathanfriend.com/projects'}             | ${'Projects'}                        | ${'.content-section h1'}           | ${'Projects'}
      ${'https://nathanfriend.com/all-posts'}            | ${'Nathan Friend'}                   | ${'.content-section h1'}           | ${'Blog'}
      ${'https://nathanfriend.com/gitlab-contributions'} | ${'My GitLab Contributions in a 🥜'} | ${'.content-section h1'}           | ${'My GitLab Contributions in a 🥜'}
      ${'https://nathanfriend.com/attributions'}         | ${'Attributions'}                    | ${'.content-section h1'}           | ${'Attributions'}
      ${'https://inspiral-web.nathanfriend.com'}         | ${'Inspiral Web'}                    | ${'.gear-label'}                   | ${'24'}
      ${'https://roggle.nathanfriend.com'}               | ${'Roggle'}                          | ${'h1'}                            | ${'Roggle'}
      ${'https://cooltojs.nathanfriend.com'}             | ${'CoolToJS'}                        | ${'h1'}                            | ${'CoolToJS'}
      ${'https://theremin.nathanfriend.com'}             | ${'Theremin'}                        | ${'h1'}                            | ${'Theremin'}
      ${'https://origins.nathanfriend.com'}              | ${'Nathan Friend: Origins'}          | ${'#origins-overlay > div'}        | ${'Use the arrow keys And ENTER to select a file to run:'}
      ${'https://webgl-chess.nathanfriend.com'}          | ${'WebGL Chess'}                     | ${'canvas'}                        | ${''}
      ${'https://nfjs.nathanfriend.com'}                 | ${'NF.js Demo'}                      | ${'h1'}                            | ${'todo'}
      ${'https://mandelbrot.nathanfriend.com'}           | ${'CMSC 305 Project 1: Fractals'}    | ${'.modal-header h3'}              | ${'Welcome!'}
      ${'https://rook.nathanfriend.com'}                 | ${'In loving memory of Rook'}        | ${'h1'}                            | ${'Rook is no more! :('}
      ${'https://nathanfriend.com/ahholyjesus/'}         | ${'Ah, Holy Jesus'}                  | ${'#lyrics-container p'}           | ${'Ah, Holy Jesus'}
      ${'https://nathanfriend.com/site_archive/'}        | ${'Site Archive'}                    | ${'h1'}                            | ${'The History of nathanfriend.com'}
      ${'https://nathanfriend.com/WordCloud/'}           | ${'SoyRIM Word Cloud'}               | ${'#soybean-container > canvas'}   | ${''}
      ${'https://nathanfriend.com/wedding/'}             | ${'Bethany and Nathan'}              | ${'.title-container'}              | ${'BETHANYandNATHAN'}
      ${'https://nathanfriend.com/speller/'}             | ${'Speller'}                         | ${'h2'}                            | ${'Speller'}
      ${'https://nathanfriend.com/portfolio/'}           | ${'Portfolio'}                       | ${'.portfolio-content > p > span'} | ${'Welcome.'}
      ${'https://nathanfriend.com/NodeChat/'}            | ${'NodeChat'}                        | ${'.modal-header'}                 | ${'Welcome to NodeChat'}
      ${'https://nathanfriend.com/battleship/'}          | ${'Battleship'}                      | ${'#grid .cell[title="alpha"]'}    | ${'A'}
      ${'https://nathanfriend.com/rookkeeper/'}          | ${'Rookkeeper'}                      | ${'h1'}                            | ${'Rookkeeper'}
    `(
      '$url returns a 200, has a page title of $title, and includes a $selector content with the text $text',
      async ({ url, title, selector, text: expectedText }) => {
        const response = await page.goto(url);

        expect(response.status()).toBe(200);

        expect(await page.title()).toBe(title);

        await page.waitForSelector(selector);

        const elementText = (await getElementText(selector)).trim();
        expect(elementText).toBe(expectedText);
      },
    );
  });

  describe('files', () => {
    it.each`
      url
      ${'https://nathanfriend.com/battleship.pdf'}
      ${'https://nathanfriend.com/LegoGitLabTanukiInstructions.pdf'}
    `('$url returns a 200', async ({ url }) => {
      const response = await axios.get(url);

      expect(response.status).toBe(200);
    });
  });
});
